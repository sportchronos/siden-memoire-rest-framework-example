﻿namespace TestContracts.Requests
{
    public class StationRequest
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public int CommuneId { get; set; }
    }
}