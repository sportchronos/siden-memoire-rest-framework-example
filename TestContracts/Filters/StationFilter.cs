﻿namespace TestContracts.Filters
{
    public class StationFilter
    {
        public bool Communes { get; set; }

        public int? CommuneId { get; set; }

        public string Page { get; set; }
    }
}