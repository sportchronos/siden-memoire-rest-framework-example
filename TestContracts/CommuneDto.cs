﻿namespace TestContracts
{
    /// <summary>
    /// A DTO class holding information about a commune
    /// </summary>
    public class CommuneDto
    {
        /// <summary>
        /// A unique identifier for a commune
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// A friendly name for the commune
        /// </summary>
        public string Name { get; set; }
    }
}