﻿namespace TestContracts
{
    public class StationDto
    {
        /// <summary>
        /// A unique Identifier for a station
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// A logical user provided but unique Code for a station.Codes typically start with U followed by 4 digits like U1206
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// A friendly name for the station
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The Id of the commune where this station belongs to
        /// </summary>
        public int CommuneId { get; set; }

        /// <summary>
        /// A full commune object this station belongs to
        /// </summary>
        public CommuneDto Commune { get; set; }
    }
}