﻿using Microsoft.AspNet.SignalR.Client;
using Refit;
using Siden.DarkKnight.Contracts.DTO;
using Siden.DarkKnight.Facade.Credentials;
using Siden.REST.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TestContracts;
using TestContracts.Filters;
using TestContracts.Requests;
using TestProxy;

namespace TestClient
{
    class Program
    {
        static StationsProxy Proxy { get; set; }
        static MyApi Api { get {  return Proxy?.Api; } }


        static void Main(string[] args)
        {
            Task.Run(() => MainAsync(args)).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            // Create Proxy for REST Server calls
            Proxy = new StationsProxy(false);   // DEV



            // Authenticate (via DarkKnight in the background)
            ICredentials credentials = new WindowsCredentials();  // Current Windows User
            TokenDto token = Proxy.Login(credentials);            // Login and receive a TokenDto Object on success
            Console.WriteLine($"Hello {token.User.FirstName} ;-)");   // Say Hello
            Console.WriteLine();

            // Init SignalR
            Proxy.InitSignalR("StationsHub", OnSubscribeToHub).Wait();



            // Show Stations by making the GET Request to /api/stations
            await ShowStations();
            Console.Write("Create new station (Code,Name,CommuneId): ");


            // Add new Stations
            string input = null;
            do
            {
                
                input = Console.ReadLine();
                if (input != "")
                {
                    List<string> words = input.Split(',').Select(x => x.Trim()).ToList();
                    if (words.Count == 3)
                    {
                        int communeId = 0;
                        if (int.TryParse(words[2], out communeId))
                        {
                            await SaveStation(words);
                            
                        }

                    }
                }
            } while (!string.IsNullOrEmpty(input));

            // Bye
            Proxy.Logout();
        }

        static void ShowError(System.Net.HttpStatusCode statusCode, string reasonPhrase)
        {
            ConsoleColor color = Console.ForegroundColor;
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"{(int)statusCode} {statusCode} - {reasonPhrase}");
            Console.WriteLine();
            Console.ForegroundColor = color;
        }

        static async Task SaveStation(List<string> words)
        {
            try
            {
                StationRequest request = new StationRequest { Code = words[0], Name = words[1], CommuneId = int.Parse(words[2]) };
                ApiResponse<StationDto> response = await Api.Stations.Post(request);

                if (response.IsSuccessStatusCode)
                {
                    //await ShowStations();
                    Console.WriteLine($"Station {response.Content.Code} ({response.Content.Name}) has been created for commune {response.Content.Commune.Name}");
                    Console.WriteLine();

                }
                else
                    ShowError(response.StatusCode, response.ReasonPhrase);
            }
            catch (Exception ex)
            {
                int p1 = ex.Message.IndexOf("\"");
                int p2 = ex.Message.IndexOf("\"", p1 + 1);

                ShowError(System.Net.HttpStatusCode.Unauthorized, ex.Message.Substring(p1+1, p2-p1-1));
            }
        }

        static SemaphoreSlim semaphoreSlim = new SemaphoreSlim(1, 1);

        static async Task ShowStations()
        {
            try
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.CursorTop = 0;
                Console.Clear();
                Console.WriteLine($"Hello {Proxy.Token.User.FirstName} ;-)");
                Console.WriteLine();
                Console.WriteLine("Stationen");
                Console.WriteLine("=========");
                var response = await Api.Stations.GetMany(new StationFilter { Communes = true });
                List<StationDto> stations = response.Content;
                foreach (var station in stations)
                    Console.WriteLine($"[{station.Code}] {station.Name} in Commune {station.Commune.Name}");
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                int p1 = ex.Message.IndexOf("\"");
                int p2 = ex.Message.IndexOf("\"", p1 + 1);

                ShowError(System.Net.HttpStatusCode.Unauthorized, ex.Message.Substring(p1 + 1, p2 - p1 - 1));
            }

            Console.Write("Create new station (Code,Name,CommuneId): ");
        }

        static void OnSubscribeToHub(object sender, SubcribeToHubEventArgs e)
        {
            e.Hub.On<StationDto>("ReceiveStationCreated", OnStationCreated);
        }

        static void OnStationCreated(StationDto message)
        {
            semaphoreSlim.WaitAsync().Wait();
            try
            {
                ShowStations().Wait();
                int x = Console.CursorLeft;
                int y = Console.CursorTop;
                ConsoleColor foreColor = Console.ForegroundColor;
                ConsoleColor backColor = Console.BackgroundColor;

                Console.SetCursorPosition(0, 0);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.BackgroundColor = ConsoleColor.Blue;

                Console.SetCursorPosition(0, Console.WindowHeight - 2);

                // Output new world
                Console.Write($"  -->  {message.Name}  <--".PadRight(Console.WindowWidth));

                Console.ForegroundColor = foreColor;
                Console.BackgroundColor = backColor;

                Console.SetCursorPosition(x, y);
            }
            finally
            {
                semaphoreSlim.Release();
            }
        }
    }
}