﻿using Refit;
using Siden.DarkKnight.Contracts;
using Siden.REST.Proxy;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using TestContracts;
using TestContracts.Filters;
using TestContracts.Requests;

namespace TestProxy
{
    public class StationsProxy : RestSidenAuthenticatedProxy<MyApi>
    {
        public StationsProxy(bool isProdEnvironment)
            : base(9999, isProdEnvironment)
        {
        }

        protected override MyApi InitApi(HttpClient client)
        {
            return new MyApi(client);
        }
    }




    public class MyApi
    {
        public IStationsApi Stations { get; private set; }

        public MyApi(HttpClient client)
        {
            Stations = RestService.For<IStationsApi>(client);
        }
    }


    public interface IStationsApi
    {
        [SidenHeaders]
        [Get("/stations/")]
        Task<ApiResponse<List<StationDto>>> GetMany(StationFilter stationFilter);


        [SidenHeaders]
        [Post("/stations/")]
        Task<ApiResponse<StationDto>> Post([Query] StationRequest request);

    }
}