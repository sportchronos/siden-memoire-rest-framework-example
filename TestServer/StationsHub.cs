﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using TestContracts;

namespace TestServer
{
    [HubName("StationsHub")]
    public class StationsHub : Hub
    {
        public void NewStationCreated(StationDto station)
        {
            // Dispatch Message to clients
            Clients.All.ReceiveStationCreated(station);
        }
    }
}