﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    public static class AutomapperInitialization
    {
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //cfg.CreateMap<DAL.SidenGest.GmEquipeContact, TeamContactModel>()
                //      .ForMember(d => d.Guid, o => o.MapFrom(s => s.EquipeContactGuid))
                //      .ForMember(d => d.IsActive, o => o.MapFrom(s => !s.EstSupprime))
                //      .ForMember(d => d.ContactGuid, o => o.MapFrom(s => s.Contact.ContactGuid))
                //      .ForMember(d => d.TeamGuid, o => o.MapFrom(s => s.GmEquipe.EquipeGuid));


                cfg.AddProfiles(typeof(AutomapperInitialization).Assembly);
            });

           

            return new Mapper(config);
        }
        
    }

}
