﻿using Autofac;
using AutoMapper;
using Serilog;
using Siden.REST.Server;

namespace TestServer
{
    public class AutofacBuilder : AutofacBuilderBase
    {
        public AutofacBuilder(RestConfig restConfig) 
            : base(restConfig)
        {

        }


        protected override void Register(ContainerBuilder builder)
        {
            // Dont' forget this base Call... It is registering the IIsLiveProvider and the IHubProxy
            base.Register(builder);

            builder.Register(c => AutomapperInitialization.GetMapper()).As<IMapper>().SingleInstance();

            ILogger log = new LoggerConfiguration()
                           .Enrich.FromLogContext()
                           .WriteTo.Console(Serilog.Events.LogEventLevel.Information)
                           .CreateLogger();
            Serilog.Log.Logger = log;

            builder.Register(c => log).As<ILogger>().SingleInstance();
        }
    }
}
