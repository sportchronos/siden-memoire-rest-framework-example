﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Controllers;
using System.Collections.ObjectModel;
using Siden.REST.Server.Help;
using Siden.REST.Server;


namespace TestServer
{
    public class Startup : StartupBase
    {
        protected override AutofacBuilderBase GetAutofacBuilder()
        {
            return new AutofacBuilder(RestConfig);
        }
        
        protected override void AddServices(ServicesContainer services)
        {
            services.Add(typeof(IExceptionLogger), new ConsoleExceptionLogger());
        }

        protected override void AddMessageHandlers(Collection<DelegatingHandler> messageHandlers)
        {
            // Ommit default Requests Logging
            // base.AddMessageHandlers(messageHandlers);
        }

        protected override void RegisterHelp(HttpConfiguration config)
        {
            config.SetDocumentationProvider(new XmlDocumentationProvider(new string[] { "TestServer.xml" }));
        }

        protected override void MapRoutes(HttpRouteCollection routes)
        {
            base.MapRoutes(routes);
        }
        
    }
}