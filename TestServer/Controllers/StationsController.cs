﻿using AutoMapper;
using Microsoft.AspNet.SignalR.Client;
using Serilog;
using Siden.DarkKnight.Utils.Attributes;
using Siden.REST.Server.Controllers;
using Siden.Tools.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using TestContracts;
using TestContracts.Filters;
using TestContracts.Requests;
using TestServer.Extensions;

namespace TestServer
{
    public class StationsController : SidenApiController
    {
        readonly IMapper _mapper;
        readonly IHubProxy _hub;
        readonly ILogger _logger;
        public StationsController(IIsLiveProvider isLiveProvider, IMapper mapper, IHubProxy hub, ILogger logger)
            : base(isLiveProvider)
        {
            this._logger = logger;
            this._hub = hub;
            this._mapper = mapper;
        }


        /// <summary>
        /// Returns Stations according to filter
        /// </summary>
        [PolicyCheck("Read Stations")]
        [SidenToken]
        [HttpGet]
        public async Task<IHttpActionResult> GetMany([FromUri] StationFilter filter)
        {
            var stations = StationProvider.GetMany(filter);
            var envelope = new PagingEnvelope<StationDto>(stations, filter.Page);

            _logger.Information("Stations have been loaded");
            return Ok(envelope.Values).AddHeader(envelope);
        }

        /// <summary>
        /// Create a new station
        /// </summary>
        [PolicyCheck("Create Stations")]
        [SidenToken]
        [HttpPost]
        public async Task<IHttpActionResult> Post(StationRequest request)
        {
            try
            {
                StationDto createdStation = StationProvider.Add(request);

                if (createdStation != null)
                {
                    // Send Hub Message
                    await _hub.Invoke("NewStationCreated", createdStation);
                }
                return Ok(createdStation);
            }
            catch (Exception ex)
            {
                _logger.Error($"[{ex.GetType().Name}] {ex.Message}");
                IHttpActionResult result = BadRequest().With(ex.Message);
                return result;
                
            }
            
        }
    }

    public static class StationProvider
    {
        private static List<StationDto> list = new List<StationDto>()
        {
            new StationDto { Id = 1, Code = "U1234", Name = "KA Bleesbruck", CommuneId = 1 },    // Diekirch District
            new StationDto { Id = 2, Code = "U5678", Name = "RUB Diekirch", CommuneId = 2 },  // Vianden District
            new StationDto { Id = 3, Code = "U4711", Name = "KA Wiltz", CommuneId = 3 }      // Diekirch District
        };

        public static List<StationDto> GetMany(StationFilter filter)
        {
            IEnumerable<StationDto> stations = list;
            if(filter.CommuneId != null)
                stations = from x in stations where x.CommuneId == filter.CommuneId.Value select x;

            if (filter.Communes)
            {
                var districts = CommuneProvider.GetAll().ToDictionary(x => x.Id);
                Parallel.ForEach(stations, x => x.Commune = districts[x.CommuneId]);
            }

            return stations.ToList();
        }

        public static StationDto Add(StationRequest request)
        {
            if(list.Where(x => x.Code == request.Code).Count() > 0)
                throw new Exception($"Station '{request.Code}' existiert bereits");

            CommuneDto commune = CommuneProvider.GetAll().Where(x => x.Id == request.CommuneId).FirstOrDefault();
            if (commune == null)
            {
                var communes = CommuneProvider.GetAll().Select(x => $"{x.Id}-{x.Name}").ToList();
                string choices = string.Join(", ", communes);
                throw new Exception($"Gemeinde {request.CommuneId} existiert nicht. Auswählen aus {{ {choices} }}");
            }

            StationDto station = new StationDto
            {
                Id = GetMany(new StationFilter()).Select(x => x.Id).Max(),
                Code = request.Code,
                Name = request.Name,
                CommuneId = request.CommuneId,
                Commune = commune
            };
            list.Add(station);
            return station;
        }
    }

    public static class CommuneProvider
    {
        public static List<CommuneDto> GetAll()
        {
            List<CommuneDto> list = new List<CommuneDto>();
            list.Add(new CommuneDto { Id = 1, Name = "Bettendorf" });
            list.Add(new CommuneDto { Id = 2, Name = "Diekirch" });
            list.Add(new CommuneDto { Id = 3, Name = "Wiltz" });

            return list;
        }
    }
}