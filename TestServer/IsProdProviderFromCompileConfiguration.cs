﻿using Siden.DarkKnight.Utils.Attributes;

namespace TestServer
{
    public class IsProdProviderFromCompileConfiguration : IIsLiveProvider
    {
        public bool IsLive
        {
            get
            {
#if DEBUG
                return false;
#else
                return true;
#endif


            }
        }
    }
}
