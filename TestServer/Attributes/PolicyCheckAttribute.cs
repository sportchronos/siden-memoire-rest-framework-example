﻿using Siden.DarkKnight.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestServer
{
    public class PolicyCheckAttribute : DefaultPolicyCheckAttribute
    {
        public PolicyCheckAttribute(string policyName)
            : base(new IsProdProviderFromCompileConfiguration(), Properties.Settings.Default.ApplicationNr, policyName)
        {

        }
    }
}
