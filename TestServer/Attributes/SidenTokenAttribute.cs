﻿using Siden.DarkKnight.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace TestServer
{
    public class SidenTokenAttribute : DefaultSidenTokenAttribute
    {
        public SidenTokenAttribute()
            : base(new IsProdProviderFromCompileConfiguration(), Properties.Settings.Default.ApplicationNr)
        {

        }

        public override Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            return base.OnAuthorizationAsync(actionContext, cancellationToken);
        }

    }
}
