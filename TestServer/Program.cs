﻿using Siden.DarkKnight.Utils.Attributes;
using Siden.REST.Server;
using System;

namespace TestServer
{
    class Program
    {
        // netsh http add urlacl url=https://+:49002/ user=peiffer
        // netsh http add sslcert ipport=0.0.0.0:49002 certhash=ff9a58aa87fbb2f42aebedd069802a7fb60ccebd appId={3c147425-c8c5-40bc-ac1d-935278d5d8df}

        static void Main(string[] args)
        {
            IIsLiveProvider isLiveProvider = new IsProdProviderFromCompileConfiguration();
            Uri baseUri = new Uri(Properties.Settings.Default.BaseUrl);

            RestConfig config = new RestConfig(baseUri, isLiveProvider)
            {
                Title = "Test REST API Server",
                EventSource = "Test REST API Server",
                EnableSignalR = true,
                HubName = "StationsHub"
            };

            var launcher = new WebApiLauncher<Startup>(config);

            launcher.Execute();
        }
    }
}